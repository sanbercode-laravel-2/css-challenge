# layouting-html-css

## Target
* Membuat layout HTML dengan CSS

## Petunjuk Pengerjaan
1. Membuat File HTML
Buatlah sebuah file HTML bernama index.html. Gunakan code yang sudah disediakan di bawah ini:

```html 

<html>
  <head>
    <link href="public/css/style.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Slabo+27px" rel="stylesheet">
  </head>
  <body>
    <header>
      <img id="logo" src="public/img/logo.png" width="200px" />
      <nav>
        <ul>
          <a href="#"><li>Home</li></a>
          <a href="#"><li>About</li></a>
          <a href="#"><li>Contact</li></a>
        </ul>
      </nav>
    </header>
    <section>
      <h1>Featured Posts</h1>
      <div id="article-list">
        <article>
          <a href=""><h3>Lorem Post 1</h3></a>
          <p>
            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
        </article>
        <article>
          <a href=""><h3>Lorem Post 2</h3></a>
          <p>
            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
        </article>
        <article>
          <a href=""><h3>Lorem Post 3</h3></a>
          <p>
            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
        </article>
        <article>
          <a href=""><h3>Lorem Post 4</h3></a>
          <p>
            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
        </article>
        <article>
          <a href=""><h3>Lorem Post 5</h3></a>
          <p>
            Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
          </p>
        </article>
      </div>
    </section>
    <footer>
      <h5>copyright &copy; 2019 by Sanbercode</h5>
    </footer>
  </body>
</html>
```

2. Buat File CSS

Buatlah file CSS di dalam folder bernama 'css'. folder 'css' tersebut berada di dalam folder public.
kemudian hubungkan pada file index.html dengan cara menambahkannya di header. 
```html
<link href="public/css/style.css" rel="stylesheet" type="text/css">
```
3. Menyiapkan Asset Gambar

Buatlah folder di dalam folder public dengan nama img dimana kita akan meletakkan gambar kita di sana. Aset gambar bisa kamu download dari file di bawah ini: 

![Sanbercode Logo](logo.png)
![Sanbercode Logo](pattern.jpg)

keterangan: logo sanbercode untuk logo yang akan ditampilkan. sedangkan gambar pola yang di bawahnya adalah gambar yang dijadikan background.

4. Membuat Layout HTML + CSS
Buatlah halaman web dengan layout seperti di bawah ini: 
![Simulasi](simulated.gif)
 




